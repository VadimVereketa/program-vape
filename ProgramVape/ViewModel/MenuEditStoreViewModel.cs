﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ProgramVape.Model;

namespace ProgramVape.ViewModel
{
    public class MenuEditStoreViewModel : ViewModelBase
    {
        private ObservableCollection<Store> _stores;
        private Store _currentStore;
        private ICommand _addStoreCommand;
        private ICommand _deleteStoreCommand;

        public ObservableCollection<Store> Stores
        {
            get { return this._stores; }
            set
            {
                this._stores = value;
                OnPropertyChanged();
            }
        }

        public Store CurrentStore
        {
            get
            {
                return _currentStore;
            }

            set
            {
                this._currentStore = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddStoreCommand
        {
            get
            {
                return _addStoreCommand ?? new RelayCommand(() =>
                {
                    Stores.Add(new Store());
                });
            }
        }

        public ICommand DeleteStoreCommand
        {
            get
            {
                return _deleteStoreCommand ?? new RelayCommand(() =>
                {
                    Stores.Remove(CurrentStore);
                    CurrentStore = null;
                    
                }, o => CurrentStore != null);
            }
            
        }

        public MenuEditStoreViewModel(ObservableCollection<Store>  stores)
        {
            Stores = stores;
        }
    }
}