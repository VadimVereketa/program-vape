﻿using System;
using System.Windows.Input;

namespace ProgramVape.ViewModel
{
    public class RelayCommand : ICommand
    {
        private Action action = null;
        private Predicate<Object> predicate = null;

        public RelayCommand(Action action, Predicate<Object> predicate = null)
        {
            this.action = action;
            this.predicate = predicate;
        }
        public bool CanExecute(object parameter) => this.predicate?.Invoke(parameter) ?? true;

        public void Execute(object parameter) => this.action?.Invoke();

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

    public class RelayCommand<T> : ICommand
    {

        private Action<T> action = null;
        private Predicate<T> predicate = null;

        public RelayCommand(Action<T> action, Predicate<T> predicate = null)
        {
            this.action = action;
            this.predicate = predicate;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter) => this.predicate?.Invoke((T)parameter) ?? true;

        public void Execute(object parameter) => this.action?.Invoke((T)parameter);
    }
}