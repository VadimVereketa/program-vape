﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ProgramVape.Helper;
using ProgramVape.Model;

namespace ProgramVape.ViewModel
{
    public class AddOrderViewModel : ViewModelBase
    {
        private ObservableCollection<Product> _products;
        private ObservableCollection<Store> _stores; 
        private DateTime _deliveryDate;
        private Double _costOfDilivery;
        private Boolean _isStart;
        private Store _store;
        private Product _selectProduct;

        private ICommand _addProductCommand;
        private ICommand saveOrderCommand;
        private ICommand _startCommand;
        private ICommand _deleteCommand;
        private MenuEditStoreViewModel _menuEditStore;

        public AddOrderViewModel()
        {
            DeliveryDate = DateTime.Now;
            CostOfDilivery = .0;

            var collection = new List<Store>(new[]
            {
                new Store() {Address = "Addres", Name = "NameStore", Phone = "096 186 1315", Website = "http:\\ex.ua"},
            });
            var products = new List<Product>(new Product[]
            {
                new Basis()
                {
                    Brend = "Brend", Milliliters = 100, Nicotine = 6, PG = 50, VG = 50, Price = 56,
                },
                new Flavoring()
                {
                    Brend = "Brend",
                    Milliliters = 30,
                    Price = 35,
                    
                    Concentration = new Concentration() {Drops = 40, PerMilliliters = 30},
                },
                new Vaporizer() {Brend = "Brend", Price = 60, Resistance = 18,}, 
            });


            Products = new ObservableCollection<Product>(products);
            Stores = new ObservableCollection<Store>(collection);

            MenuEditStore = new MenuEditStoreViewModel(Stores);
        }
        public ObservableCollection<Product> Products
        {
            get
            {
                return _products;
            }

            set
            {
                this._products = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddProductCommand
        {
            get
            {
                return _addProductCommand ?? new RelayCommand<String>(s =>
                {
                    TypeProduct typeProduct;
                    if (Enum.TryParse(s, out typeProduct))
                    {
                        Products.Add(ProductFactory.CreateProduct(typeProduct));
                    }
                });
            }
        }

        public Store Store
        {
            get { return this._store; }
            set
            {
                this._store = value;
                OnPropertyChanged();
            }
        }

        public DateTime DeliveryDate
        {
            get
            {
                return _deliveryDate;
            }

            set
            {
                this._deliveryDate = value;
            }
        }

        public double CostOfDilivery
        {
            get
            {
                return _costOfDilivery;
            }

            set
            {
                this._costOfDilivery = value;
            }
        }

        public ICommand SaveOrderCommand
        {
            get { return saveOrderCommand ?? new RelayCommand(() =>
            {
                Order order = new Order();
                foreach (Product product in this._products)
                {
                    product.Order = order;
                }
                order.Products = new List<Product>(this._products);
                order.DeliveryDate = DeliveryDate;
                order.CostOfDilivery = CostOfDilivery;
                order.Store = Store;
               
                //Save DataBase
                Clear();
            });
        }
            
        }

        public ICommand StartCommand => this._startCommand ?? new RelayCommand(() => IsStart = true);

        public bool IsStart
        {
            get
            {
                return _isStart;
            }

            set
            {
                this._isStart = value;
                OnPropertyChanged();
            }
        }

        public MenuEditStoreViewModel MenuEditStore
        {
            get
            {
                return _menuEditStore;
            }

            set
            {
                this._menuEditStore = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Store> Stores
        {
            get
            {
                return _stores;
            }

            set
            {
                this._stores = value;
                OnPropertyChanged();
            }
        }

        public Product SelectProduct
        {
            get
            {
                return _selectProduct;
            }

            set
            {
                this._selectProduct = value;
                OnPropertyChanged();
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? new RelayCommand<Product>(product =>
                {
                    Products.Remove(product);
                });
            }
        }

        private void Clear()
        {
            Products = null;
            CostOfDilivery = 0;
            IsStart = false;
            Store = null;
        }
    }
}