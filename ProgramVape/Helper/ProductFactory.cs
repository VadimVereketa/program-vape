﻿using System;
using ProgramVape.Model;

namespace ProgramVape.Helper
{
    public enum TypeProduct
    {
        Basis,
        Flavoring,
        Vaporizer
    }
    public static class ProductFactory
    {
        public static Product CreateProduct(TypeProduct typeProduct)
        {
            switch (typeProduct)
            {
                case TypeProduct.Basis:
                    return new Basis();
                case TypeProduct.Flavoring:
                    return new Flavoring() {Concentration = new Concentration()};
                case TypeProduct.Vaporizer:
                    return new Vaporizer();
                default:
                    throw new ArgumentOutOfRangeException(nameof(typeProduct), typeProduct, null);
            }
        }
    }
}