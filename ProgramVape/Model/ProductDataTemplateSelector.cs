﻿using System.Windows;
using System.Windows.Controls;

namespace ProgramVape.Model
{
    public class ProductDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (element != null && item != null)
            {
                if(item is Basis)
                    return element.FindResource("BasisTemplate") as DataTemplate;
                else if(item is Vaporizer)
                    return element.FindResource("VaporizerTemplate") as DataTemplate;
                else if(item is Flavoring)
                    return element.FindResource("FlavoringTemplate") as DataTemplate;
            }

            return null;

        }
    }
}