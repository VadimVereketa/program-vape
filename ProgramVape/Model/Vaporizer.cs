using System;

namespace ProgramVape.Model
{
    class Vaporizer : Product
    {
        public Int32 Resistance { get; set; }


        public override string Kind { get; protected set; } = "Vaporizer";

    }
}