using System;

namespace ProgramVape.Model
{
    class Flavoring : Product
    {
        public Int32 Milliliters { get; set; }


        public Concentration Concentration { get; set; }

        public override string Kind { get; protected set; } = "Flavoring";
    }
}