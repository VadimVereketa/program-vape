﻿using System;
using System.Collections.Generic;

namespace ProgramVape.Model
{
    public class Order 
    {
        public DateTime DeliveryDate { get; set; }
        public List<Product> Products { get; set; }
        public Double CostOfDilivery { get; set; }//стоимость доставки


        public Store Store { get; set; }


    }
}