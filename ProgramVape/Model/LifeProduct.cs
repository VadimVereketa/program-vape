﻿using System;

namespace ProgramVape.Model
{
    public class LifeProduct
    {
        public DateTime Begin { get; set; }
        public DateTime? End { get; set; }

        public Product Product { get; set; }
    }
}