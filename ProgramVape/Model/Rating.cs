﻿using System;

namespace ProgramVape.Model
{
    public class Rating
    {
        private Int32 rating;
        private Rating() { }

        private Rating(Int32 i)
        {
            this.rating = i;
        }

        public static Rating None=> new Rating(0);
        public static Rating One=> new Rating(1);
        public static Rating Two=> new Rating(2);
        public static Rating Trhee=> new Rating(3);
        public static Rating Four=> new Rating(4);
        public static Rating Five=> new Rating(5);
    }
}