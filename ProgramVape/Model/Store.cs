using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProgramVape.Model
{
    public class Store 
    {
        public String Name { get; set; }
        public String Address { get; set; }
        public String Phone { get; set; }
        //public Rating Rating { get; set; }
        public String Website { get; set; }


        public List<Order> Orders { get; set; }
        
    }
}