﻿using System;
using System.Windows.Documents;

namespace ProgramVape.Model
{
    public abstract class Product
    {
        public Double Price { get; set; }
        public abstract String Kind { get; protected set; }
        public String Brend { get; set; }
        //public Rating Rating { get; set; }

        public LifeProduct Life { get; set; }
        public Order Order { get; set; }

    }
}