using System;

namespace ProgramVape.Model
{
    class Basis : Product
    {
        public Int32 Milliliters { get; set; }
        public Int32 Nicotine { get; set; }
        public Int32 VG { get; set; }
        public Int32 PG { get; set; }

        public override string Kind { get; protected set; } = "Base";
    }
}