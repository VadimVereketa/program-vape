using System;

namespace ProgramVape.Model
{
    class Concentration
    {
        public Int32 Drops { get; set; }
        public Int32 PerMilliliters { get; set; }
        

    }
}